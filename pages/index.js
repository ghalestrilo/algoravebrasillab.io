import Head from 'next/head'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/style.css" />
      </Head>

      <main>
        <h1 className="title">
          {/* Algorave Brasil */}
    <ul class="c-rainbow">
      {["white", "orange", "red", "violet", "blue", "green", "yellow"].map(color =>
        <li class={`c-rainbow__layer c-rainbow__layer--${color}`}>
          <h1 className="title">AlgoraveBrasil</h1>
        </li>)}
    </ul>
        </h1>

        <p className="description">
        <p>
      Live coding é uma forma de performance artística e uma técnica criativa
      centrada na escrita de código-fonte e no uso de programação interativa,
      improvisada, frequentemente aplicada na criação de sons e imagens
      digitais, sistemas de luzes, dança improvisada e poesia.
    </p>
    <p>
      Desde 2004 a comunidade internacional de Live Coding se organiza
      informalmente através do <a href="https://toplap.org">TOPLAP</a> promovendo e
      difundindo o tema no mundo, sendo ponto focal para todos os praticantes e
      pesquisadores no tema, na América Latina o coletivo <a
      href="https://colectivo-de-livecoders.gitlab.io">CLiC</a> tem feito há
      bastante tempo um ótimo trabalho de acolhimento individual e promove uma
      constante reflexão sobre o uso e desenvolvimento tecnológico e suas
      implicações políticas e sociais.
    </p>
    <p>
      No Brasil os praticantes, pesquisadores, artistas, músicos e demais
      interessados no tema se reúnem através da comunidade <b>Algorave
      Brasil</b>, promovendo eventos, trocando informações, recepcionando
      novatos, especialmente aqueles interessados e curiosos que nunca tiveram
      contato com live coding e tem interesse em conhecer ou praticar, e acima
      de tudo um espaço inclusivo, diverso e com respeito às diferenças e
      necessidades individuais de cada um.
    </p>
        </p>

      </main>

      <footer>
        Página em construção ❤️
      </footer>
    </div>
  )
}